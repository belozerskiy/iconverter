﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;


namespace iConverter
{

    public partial class Form1 : Form
    {
        public  Logger log = Logger.getInstance();
        private FileDatabase db = FileDatabase.getInstance();
        private Unit unit1;
        private Unit unit2;

        public Form1()
        {
            InitializeComponent(); 
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach(string key in db.getStorage().Keys)
            {
                units.Items.Add(key);
            }
            units.SelectedIndex = 0;
        }


        public void getErr(string err)
        {
            MessageBox.Show(err, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            log.putToLog(err);
        }

        private void convert_Click(object sender, EventArgs e)
        {
            toField.Clear();
            try
            {
               if (fromField.TextLength != 0) 
               {
                    string result = "";
                    unit1 = (Unit)db.getStorage()[units.Text][fromUnit.SelectedIndex];
                    unit2 = (Unit)db.getStorage()[units.Text][ToUnit.SelectedIndex];

                    UnitController uController = new UnitController(unit1, unit2);
                    result = uController.convert(fromField.Text).ToString("0.#########");
                    toField.Text = result;
               }
               else
               {
                    getErr( "Put data on field!");
               }
            }
            catch(Exception ex)
            {
                log.putToLog(ex);
            }
            
        }

        private void units_SelectedIndexChanged(object sender, EventArgs e)
        {
            fromUnit.Items.Clear();
            ToUnit.Items.Clear();

            foreach(Unit unit in db.getStorage()[units.Text]){
                fromUnit.Items.Add(unit.getName());
                ToUnit.Items.Add(unit.getName());
            }
            fromUnit.SelectedIndex = 0;
            ToUnit.SelectedIndex = 1;
        }

        private void fromUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void ToUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
        
    }
}
