﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;


namespace iConverter
{

    public partial class Form1 : Form
    {
       
        
        public static Logger log = new Logger();
        private FileDatabase db = FileDatabase.getInstance();
        private Unit unit1;
        private Unit unit2;

        public Form1()
        {
            InitializeComponent(); 
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach(string key in db.getStorage().Keys)
            {
                units.Items.Add(key);
            }
            units.SelectedIndex = 0;
        }

        private void convert_Click(object sender, EventArgs e)
        {
            toField.Clear();
            try
            {
               if (fromField.TextLength != 0) {
                   string result = "";

                   unit1 = (Unit)db.getStorage()[units.Text][fromUnit.SelectedIndex];
                   unit2 = (Unit)db.getStorage()[units.Text][ToUnit.SelectedIndex];

                   UnitController uController = new UnitController(unit1, unit2);

                   result = String.Format("{0:N0}", uController.convert(fromField.Text));
                   //Console.WriteLine(result);
                   toField.Text = result;

               }
               else
               {
                   
                   MessageBox.Show("Put data on field!", "Ahtung!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   
               }
            }
            catch(Exception ex)
            {
                log.putToLog(ex);
            }
            
        }

        private void units_SelectedIndexChanged(object sender, EventArgs e)
        {

            fromUnit.Items.Clear();
            ToUnit.Items.Clear();

            foreach(Unit unit in db.getStorage()[units.Text]){
                fromUnit.Items.Add(unit.getName());
                ToUnit.Items.Add(unit.getName());
            }
            fromUnit.SelectedIndex = 0;
            ToUnit.SelectedIndex = 1;
        }

        private void fromUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void ToUnit_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
        
    }
}
