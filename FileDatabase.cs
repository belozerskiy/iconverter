﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace iConverter
{
    class FileDatabase
    {
        private const string pathPrefix = "../../db/";
        private const string filePath = pathPrefix + "Units.txt";
        private Logger log = Logger.getInstance();

        private static FileDatabase instance = new FileDatabase();
        private StreamReader reader;
        private static ArrayList unitsKeys;
        private static ArrayList unitsValues;
        private static Dictionary<string, ArrayList> storage;

        public static FileDatabase getInstance()
        {
            return instance; 
        }
        protected FileDatabase(){}
        private ArrayList getUnitName()
        {
            reader = new StreamReader(filePath);
            string line = "";
            if (unitsKeys == null) {
                unitsKeys = new ArrayList();
                while((line = reader.ReadLine()) != null)
                {
                   // if (this.fileExist(line.Trim()))
                    //{
                        unitsKeys.Add(line.Trim());
                    //}
                }
            }
            reader.Close();
            return unitsKeys;
        }

        /*private bool fileExist(string line)
        {
            try
            {
                string path = pathPrefix + line + ".txt";
                Console.WriteLine(path);
                reader = new StreamReader(path);
                reader.Close();
            }
            catch(Exception ex)
            {
                return false;
            }
            return true; 
        }*/

        public void testUnitsName()
        {
            foreach (string unitName in this.getUnitName())
            {
                Console.WriteLine(unitName);
            }
        }

        private ArrayList getUnitData(string path)
        {
           try 
           {
                reader = new StreamReader(path);
                string line = "";
                unitsValues = new ArrayList();
                while ((line = reader.ReadLine()) != null)
                {
                    unitsValues.Add(new Unit(line.Split(',')));
                }
                reader.Close();
                return unitsValues;
            }
            catch(Exception ex)
            {
                log.putToLog(ex);
                return null;
            }
        }

        //test
        public void testUnitsData()
        {
            string path = pathPrefix + "Distance" + ".txt";
            foreach (Unit unit in getUnitData(path))
            {   
                Console.WriteLine(unit.getName() + "," + unit.getScale() + "," +  unit.getOffset());
            }
        }
        
        public Dictionary<string, ArrayList> getStorage()
        {
            if (storage == null) 
            { 
                storage = new Dictionary<string, ArrayList>();
                foreach (string unitName in this.getUnitName())
                {
                    string path = pathPrefix + unitName + ".txt";
                    if (this.getUnitData(path) != null) { 
                        storage.Add(unitName, this.getUnitData(path));
                    }
                }
            }
            return storage;
        }
    }
}
