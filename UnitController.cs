﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace iConverter
{
    class UnitController
    {
        private Unit unit1;
        private Unit unit2;
        private double scale1;
        private double scale2;
        private double offset1;
        private double offset2;

        public UnitController(Unit unit1, Unit unit2)
        {
            this.unit1 = unit1;
            this.unit2 = unit2;

            try
            {
                scale1 = Double.Parse(this.unit1.getScale(), CultureInfo.InvariantCulture);
                scale2 = Double.Parse(this.unit2.getScale(), CultureInfo.InvariantCulture);
                offset1 = Double.Parse(this.unit1.getOffset(), CultureInfo.InvariantCulture);
                offset2 = Double.Parse(this.unit2.getOffset(), CultureInfo.InvariantCulture);

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
        }

        public double convert(string data){
            double result = 0.0;
            double data1 = Double.Parse(data, CultureInfo.InvariantCulture);

            /* 
             if(unit1.getName() != unit2.getName())
             {
                 if (this.unit2.getFormula().Count != 0)
                 {
                     double a;
                

                     foreach(string str in this.unit2.getFormula()){
                         string s;
                         if (str.IndexOf("F=") != -1)
                         {
                             s = str.Replace("F=", "");
                             s = s.Replace("C", data);
                             a = (data1 * 1.8 + 32);
                             result = a;
                         }
                     }
                 }
                 else if (this.unit1.getFormula().Count != 0)
                 {
                     double a;

                     foreach (string str in this.unit1.getFormula())
                     {
                         string s;
                         if (str.IndexOf("C=") != -1)
                         {
                             s = str.Replace("C=", "");
                             s = s.Replace("F", data);
                             a = (data1 - 32 / 1.8);
                             Console.WriteLine(a);
                             result = a;
                         }
                     }

                 }
             }

             else 
             {     
             }*/

                result = ((data1) * scale1) - offset1;
                result = ((result) / scale2) + offset2;
            
            return result;
        }

    }
}
