﻿namespace iConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convert = new System.Windows.Forms.Button();
            this.units = new System.Windows.Forms.ComboBox();
            this.fromUnit = new System.Windows.Forms.ComboBox();
            this.ToUnit = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fromField = new System.Windows.Forms.TextBox();
            this.toField = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // convert
            // 
            this.convert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.convert.ForeColor = System.Drawing.Color.Green;
            this.convert.Location = new System.Drawing.Point(96, 128);
            this.convert.Name = "convert";
            this.convert.Size = new System.Drawing.Size(104, 31);
            this.convert.TabIndex = 0;
            this.convert.Text = "Convert";
            this.convert.UseVisualStyleBackColor = true;
            this.convert.Click += new System.EventHandler(this.convert_Click);
            // 
            // units
            // 
            this.units.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.units.FormattingEnabled = true;
            this.units.Location = new System.Drawing.Point(112, 32);
            this.units.Name = "units";
            this.units.Size = new System.Drawing.Size(88, 21);
            this.units.TabIndex = 1;
            this.units.SelectedIndexChanged += new System.EventHandler(this.units_SelectedIndexChanged);
            // 
            // fromUnit
            // 
            this.fromUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromUnit.FormattingEnabled = true;
            this.fromUnit.Location = new System.Drawing.Point(160, 64);
            this.fromUnit.Name = "fromUnit";
            this.fromUnit.Size = new System.Drawing.Size(113, 21);
            this.fromUnit.TabIndex = 1;
            this.fromUnit.SelectedIndexChanged += new System.EventHandler(this.fromUnit_SelectedIndexChanged);
            // 
            // ToUnit
            // 
            this.ToUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ToUnit.FormattingEnabled = true;
            this.ToUnit.Location = new System.Drawing.Point(160, 96);
            this.ToUnit.Name = "ToUnit";
            this.ToUnit.Size = new System.Drawing.Size(113, 21);
            this.ToUnit.TabIndex = 1;
            this.ToUnit.SelectedIndexChanged += new System.EventHandler(this.ToUnit_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(64, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Units";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(8, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "From:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(24, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "To:";
            // 
            // fromField
            // 
            this.fromField.BackColor = System.Drawing.SystemColors.Window;
            this.fromField.ForeColor = System.Drawing.SystemColors.WindowText;
            this.fromField.Location = new System.Drawing.Point(56, 64);
            this.fromField.Name = "fromField";
            this.fromField.Size = new System.Drawing.Size(100, 20);
            this.fromField.TabIndex = 3;
            // 
            // toField
            // 
            this.toField.HideSelection = false;
            this.toField.Location = new System.Drawing.Point(56, 96);
            this.toField.Name = "toField";
            this.toField.ReadOnly = true;
            this.toField.Size = new System.Drawing.Size(100, 20);
            this.toField.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Created by Michael Belozerskiy aka Dark inc.";
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(294, 198);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.toField);
            this.Controls.Add(this.fromField);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ToUnit);
            this.Controls.Add(this.fromUnit);
            this.Controls.Add(this.units);
            this.Controls.Add(this.convert);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "iConverter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button convert;
        private System.Windows.Forms.ComboBox units;
        private System.Windows.Forms.ComboBox fromUnit;
        private System.Windows.Forms.ComboBox ToUnit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fromField;
        private System.Windows.Forms.TextBox toField;
        private System.Windows.Forms.Label label4;
    }
}

