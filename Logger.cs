﻿using System;
using System.IO;


public class Logger
{
    private static Logger logger;
    private const string logName = "../../logs/log.txt";
    private static StreamWriter log = null;
    private string message = "";
	    protected Logger()
	    {
                log = new StreamWriter(logName, true);
                Console.WriteLine(message);       
	    }

        public static Logger getInstance()
        {
            if (logger == null)
            {
                logger = new Logger();
            }
            return logger;
        }

    public void putToLog(Exception err){
        log.WriteLine("[" + DateTime.Now + "]" + err.Message + "\n" + err.TargetSite);
    }

    public void putToLog(string err)
    {
        log.WriteLine("[" + DateTime.Now + "]" + err.ToString());
    }

}
